

//exercice 1

int  max2(int a, int b){
    if (a > b){
        return(a);
    }
    else {
        return(b);
    }

}
int  max3(int a, int b, int c){
    if ((a > b)&&(a>c)){
        return(a);
    }
    else if ((b > c)&&(b > a)){
        return(b);
    }
    else{
        return(c);
    }

}
void testmax(){
    int a = Ut.saisirEntier();
    int b = Ut.saisirEntier();
    int max = max2(a,b);
    Ut.afficherSL(max);
    int c = Ut.saisirEntier();
    int d = Ut.saisirEntier();
    int e = Ut.saisirEntier();
    int max2 = max3(c,d,e);
    Ut.afficherSL(max2);
}
//exercice 2
void repetCarac(int nb,char car){
    for (int i=0;i<nb;i++){
        Ut.afficher(car);
    }
}

void pyramideSimple (int h,char c){
    int g = h;
    for (int i=1;i<=h;i+=2){
        for (int j = g-1;j>1;j--){
            Ut.afficher(" ");
        }
        repetCarac(i,c);
        Ut.afficherSL(" ");
        g--;
    }
}
void testpyramidesimple(){
    Ut.afficher("Saisir h : ");
    int h = (Ut.saisirEntier()*2);
    Ut.afficher("Saisir un caractere : ");
    char c = Ut.saisirCaractere();
    pyramideSimple(h,c);
}


void afficheNombresCroissants(int nb1, int nb2){
    for (int i = nb1; i<nb2;i++){
        Ut.afficher(i%10);
    }
}
void afficheNombresDecroissants(int nb1, int nb2){
    for (int i = nb1; i>=nb2;i--){
        Ut.afficher(i%10);
    }
}
void pyramideElaboree(int h){
    afficheNombresDecroissants(88,91);
}

int nbChiffres(int n){
    if (n == 0) {
        return 1;
    }

    // Compteur pour les chiffres
    int count = 0;

    // Boucle tant que n est supérieur à 0
    while (n > 0) {
        n /= 10;  // Diviser n par 10 à chaque itération
        count++;  // Incrémenter le compteur de chiffres
    }

    return count;
}
int nbChiffresDuCarre(int n) {
    n = n * n;
    return nbChiffres(n);
}
boolean nbAmis(int p, int q) {
    int somP = 0;
    int somQ = 0;

    // Calculer la somme des diviseurs propres de p
    for (int i = 1; i < p; i++) {
        if (p % i == 0) {
            somP += i;
        }
    }

    // Calculer la somme des diviseurs propres de q
    for (int i = 1; i < q; i++) {
        if (q % i == 0) {
            somQ += i;
        }
    }

    // Vérifier si p et q sont des nombres amis
    return (somP == q && somQ == p);
}

void afficheAmis(int n) {
    for (int i = 1; i < n; i++) {
        for (int j = i + 1; j < n; j++) {  // j commence à i + 1 pour éviter les doublons
            if (nbAmis(i, j)) {
                Ut.afficherSL("Les nombres " + i + " et " + j + " sont amis");
            }
        }
    }
}
int racineParfaite(int c) {
    for (int n = 1; n <= c; n++) {
        if ((n*n)==c){
            return n;
        }

    }
    return -1;
}

boolean estCarreParfait (int c){
    if (racineParfaite(c)!=-1){
        Ut.afficherSL("Carre parfait");
        return true;
    }
    else{
        Ut.afficherSL("Carre pas parfait");
        return false;
    }
}
boolean triangleRectangle (int a, int b){
    int sommeDesCarres = a * a + b * b;

    // Vérifier si cette somme est un carré parfait
    if (estCarreParfait(sommeDesCarres)) {
        int C = racineParfaite(sommeDesCarres); // C = racine de (A^2 + B^2)
        Ut.afficherSL("Les entiers " + a + " et " + b + " peuvent être les côtés d'un triangle rectangle avec C = " + C);
        return true;
    } else {
        Ut.afficherSL("Les entiers " + a + " et " + b + " ne peuvent pas former un triangle rectangle avec un C entier.");
        return false;
    }
}


int perimetre(int a, int b){

    if (triangleRectangle(a, b)) {
        int c = racineParfaite(a * a + b * b); // Calcul de l'hypoténuse C
        return a + b + c; // Calcul du périmètre
    }
    return 0; // Retourne 0 si ce n'est pas un triangle rectangle
}
int nbrTriangle(int n){
    int count = 0;

    // Parcourir toutes les combinaisons possibles pour a et b
    for (int a = 1; a < n; a++) {
        for (int b = a; b < n; b++) { // Commence à b = a pour éviter les doublons (a, b) et (b, a)
            int p = perimetre(a, b); // Utilise la fonction perimetre pour obtenir le périmètre

            // Vérifier si le périmètre est inférieur à n
            if (p > 0 && p < n) {
                count++;
            }
        }
    }
    Ut.afficherSL(count);
    return count;

}
int syracusiens(int n){
    int count = 0;
    while(n>1){
        if (n%2==0){
            n = n/2;
        }
        else{
            n = n*3 +1;
        }
        count ++;
    }
    return count ;
}
void nbSyracusiens(int n, int nbMaxOp){
    if (syracusiens(n)<=nbMaxOp){
        Ut.afficherSL("Vrai");
    }
    else{
        Ut.afficherSL("Faux");
    }
}



void main() {
    nbSyracusiens(123, 50);
}